首先向金庸前辈致敬，本教程中会提及前辈书中的一些绝技名称。

学会本章您将掌握两项绝技的入门心法，以及多项 Gitee 的进阶用法

* 绝技1：“乾坤大挪移”——（本地与 Gitee 远程仓库之间的内容腾挪之术）
* 绝技2：“一阳指”——（实现“乾坤大挪移”的秘技 Git 之指哪打哪）

欲练神功，需有 Git ，Git 指导传送门：[https://shimo.im/docs/8QPRvwgdj8DVhPJQ](https://shimo.im/docs/8QPRvwgdj8DVhPJQ)

### Gitee 进阶用法

1. **配置SSH公钥**
2. **同步 Gitee 仓库内容到本地（无需打开网页）**
3. **本地新建文件并同步至 Gitee（无需打开网页）**
4. **删除远程仓库中的指定文件（无需打开网页）**

直奔主题！四步学习，掌握“乾坤大挪移”！

## Gitee 之四步进阶

### **第一步  配置SSH公钥——本地与 Gitee 仓库连接可免登录**

要建立本地电脑与此仓库的连接，我们可以建立一个SSH公钥提升效率。

这个公钥相当于人脸识别门锁录制的一个人脸数据，之后访问远程仓库就可以不用再输入账号密码，省时省力。

#### **创建SSH公钥步骤**

1. 本地新建一个文件夹，命名为：gitee
2. 进入 gitee 文件夹，鼠标或触控板右击文件夹内空白处，点击“Git Bash Here”
![Gitee之Gitbashhere](https://images.gitee.com/uploads/images/2020/0521/164517_e10e1804_2301581.png "gitbash.png")



3. 输入下方框中的代码以生成SSHkey，输入完成后敲击回车 Enter 键，出现冒号（共三个）就回车，询问（y/n）？就输入 y 。

```plain
ssh-keygen -t rsa -C "xxxxx@xxxxx.com"   #输入个人邮箱
```

提示：*#及后面内容是注释说明，不要把这些内容输入代码行*

![Gitee新建SSH公钥](https://images.gitee.com/uploads/images/2020/0521/164606_3fcf9c33_2301581.png "仓库部署公钥2.png")



4. 继续输入以下命令，可以看到下图所示 ssh-rsa 开头的一串代码，说明生成 SSH 公钥成功

```plain
cat ~/.ssh/id_rsa.pub
```

![Gitee之SSH创建成功](https://images.gitee.com/uploads/images/2020/0521/164637_6771ee95_2301581.png "仓库部署公钥3.png")



5. 通过点击 Gitee 主页右上角头像 「设置」->「安全设置」->「SSH公钥」进行公钥添加 ，复制(全部选中后，右键 copy），将复制的 ssh-rsa 开头的内容添加到下图公钥框中。并点击确认按钮

![Gitee部署SSH](https://images.gitee.com/uploads/images/2020/0521/164707_972474b4_2301581.png "仓库部署公钥4.png")



6. 在弹出窗口中输入 Gitee 账户的登录密码点击验证并出现“您已成功添加SSH公钥”字样即可

![Gitee确认添加SSH公钥](https://images.gitee.com/uploads/images/2020/0521/164728_7b8688d1_2301581.png "仓库部署公钥5.png")

恭喜，SSH公钥已经配置完成！

未来您在本地操作 Gitee 注册账户下的远程仓库时就无需再单独输入 Gitee 的账号密码了。



### 第二步  同步 Gitee 仓库内容到本地

此处以入门教程中的 `hello-gitee` 仓库为例，我们可以通过建立文件夹 gitee 与 `hello-gitee` 仓库的连接，然后通过 git 命令把远程仓库内容下载克隆到本地。

#### **建立连接与克隆步骤**

1. 【**重要**】设置用户名与邮箱，继续在 Git Bash Here 的命令行中继续输入以下命令，每输入一行回车一次

```plain
git config --global user.name "您的 Gitee 账户姓名"
git config --global user.email "您注册/绑定的邮箱地址"
```

如果您仅有一个 Gitee 账号，可以在 Git 全局配置时使用 --global ，此后默认操作都是使用这里设置的用户名与密码进行配置

2. 找到 `hello-gitee` 远程仓库的SSH连接，仓库页面点击克隆/下载，选择 SSH 并点击复制

![Gitee复制SSH地址](https://images.gitee.com/uploads/images/2020/0521/164959_93e45f79_2301581.png "ssh地址复制.png")



3. 初始化本地仓库并克隆远程 `hello-gitee` 仓库，命令如下所示：

```plain
git init                               #初始化仓库
git remote add origin 粘贴复制的SSH地址  #建立远程连接
git clone 粘贴复制的SSH地址              #克隆远程仓库
```

提示：初始化仓库后会在本地文件夹中新建一个隐藏文件夹 .git ，windows查看设置中可以设置隐藏文件可见即可看到此文件夹
![复制Gitee仓库的SSH地址](https://images.gitee.com/uploads/images/2020/0521/165212_0c184280_2301581.png "远程克隆.png")



4. 进入 gitee 文件夹查看，再点击 `hello-gitee` 可以看到同步到本地的文件

![Gitee克隆后文件夹](https://images.gitee.com/uploads/images/2020/0521/165228_8bd4de55_2301581.png "克隆完毕.png")

![Gitee进阶之本地文件夹](https://images.gitee.com/uploads/images/2020/0521/165029_3c9e3830_2301581.png "仓库原始内容.png")



庆祝一下，您已经掌握了“乾坤大挪移”第一式——远程克隆！

### 第三步   **本地新建文件并同步至 Gitee**

本节我们将要学习通过 git 命令新建文件，并同步到远程仓库，这就行动起来吧！

#### **新建文件并同步操作步骤**

1. 在仓库 `hello-gitee` 中新建一个“记录.doc”的文档，添加至暂存区，确认添加到数据目录并命名为“新纪录”，同步到远程仓库

```plain
cd hello-gitee              #定位到 hello-gitee 文件夹
touch 记录.doc               #新建一个记录.doc文件
git add 记录.doc             #新增“记录.doc”至暂存区
git commit -m "新纪录"       #确认新增“记录.doc”至数据目录
git push -u origin master   #推送新增文件到远程仓库
```

![Gitee之本地操作命令行示意图](https://images.gitee.com/uploads/images/2020/0521/165311_a7f62ab6_2301581.png "命令行示意图.png")

说明：原先 Git Bash Here 定位的是 gitee 的文件夹，克隆操作后，此文件夹内生成了 `hello-gitee` 的文件夹，我们要新增内容并同步到远程仓库，需要在 `hello-gitee` 文件夹中进行。

2. 确认远程仓库中是否新增了“记录.doc”的内容

![Gitee上传新文件纪录](https://images.gitee.com/uploads/images/2020/0521/165345_b2702358_2301581.png "确认上传新文件记录.png")



3. 修改新增的文件后，继续执行 `git add` 开始的命令即可同步至远程仓库

太棒了，还剩一步您就可以完成本教程，获得功力精进了！

### 第四步   **删除远程仓库中的指定文件**

如果仓库中有多余文件想要删除，如何远程删除呢？ Git 提供了非常简单的操作指令 `git rm + 文件名`。

我们创建的 hello-gitee 仓库中有一个 README.en.md的文档，我们就以它为例，看看如何来删除吧！

#### **删除仓库指定文件操作步骤**

1. 确保本地仓库与远程仓库内容一致
2. 在本地用命令删除想要删除的文件 “README.en.md”并确认本次删除操作
3. 推送到远程仓库，完成指定文件删除

具体操作可按下方代码操作：

```plain
git pull                    #同步远程仓库到本地
rm README.en.md             #删除本地文件
git commit -m "delete"      #确认删除并备注“delete”
git push                    #删除操作同步到远程仓库
```
![Gitee删除远程文件命令行示意](https://images.gitee.com/uploads/images/2020/0521/165406_01300336_2301581.png "实操截图.png")




作为实用主义接班人，您现在已经可以自由使用 Gitee 进行想要的远程同步操作了。

布置一个小作业：本地直接编辑 “记录.doc” 文档，同步至远程仓库，检查一下是否已经掌握“乾坤大挪移”！

如果对于 Git 的“一阳指”心法不过瘾，还可以继续修炼更上乘的“六脉神剑”，传送门：XXXXX

